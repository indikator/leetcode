package main

import (
	"reflect"
	"testing"
)

func Test_addTwoNumbers(t *testing.T) {
	type args struct {
		l1 *ListNode
		l2 *ListNode
	}
	tests := []struct {
		name string
		args args
		want *ListNode
	}{
		{"Test 1", args{&ListNode{Val: 2, Next: &ListNode{Val: 4, Next: &ListNode{Val: 3}}}, &ListNode{Val: 5, Next: &ListNode{Val: 6, Next: &ListNode{Val: 4}}}}, &ListNode{Val: 7, Next: &ListNode{Val: 0, Next: &ListNode{Val: 8}}}}, // [8,9,9,9,0,0,0,1]
		{"Test 2", args{&ListNode{Val: 0}, &ListNode{Val: 0}}, &ListNode{Val: 0}},
		{"Test 3", args{&ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9}}}}}}}, &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9}}}}}, &ListNode{Val: 8, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 0, Next: &ListNode{Val: 0, Next: &ListNode{Val: 0, Next: &ListNode{Val: 1}}}}}}}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := addTwoNumbers(tt.args.l1, tt.args.l2); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("addTwoNumbers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_addTwoNumbersWithExtraOne(t *testing.T) {
	type args struct {
		l1       *ListNode
		l2       *ListNode
		extraOne bool
	}
	tests := []struct {
		name string
		args args
		want *ListNode
	}{
		{"Test 1", args{&ListNode{Val: 0}, &ListNode{Val: 0}, false}, &ListNode{Val: 0}},
		{"Test 2", args{&ListNode{Val: 0}, &ListNode{Val: 0}, true}, &ListNode{Val: 1}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := addTwoNumbersWithExtraOne(tt.args.l1, tt.args.l2, tt.args.extraOne); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("addTwoNumbersWithExtraOne() = %v, want %v", got, tt.want)
			}
		})
	}
}
