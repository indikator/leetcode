package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	l1 := ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9}}}}}}}
	l2 := ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9, Next: &ListNode{Val: 9}}}}
	l := addTwoNumbers(&l1, &l2)
	for l != nil {
		fmt.Println(l.Val)
		l = l.Next
	}
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	return addTwoNumbersWithExtraOne(l1, l2, false)
}

func addTwoNumbersWithExtraOne(l1 *ListNode, l2 *ListNode, extraOne bool) *ListNode {
	result := &ListNode{}
	s := l1.Val + l2.Val
	if extraOne {
		s++
	}

	result.Val = s % 10
	if l1.Next != nil || l2.Next != nil || s >= 10 {
		if l1.Next == nil {
			l1.Next = &ListNode{}
		}
		if l2.Next == nil {
			l2.Next = &ListNode{}
		}

		result.Next = addTwoNumbersWithExtraOne(l1.Next, l2.Next, s >= 10)
	}

	return result
}
