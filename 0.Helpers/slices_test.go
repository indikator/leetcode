package helpers

import "testing"

func TestSlicesEqualsByContentInt(t *testing.T) {
	type args struct {
		s1 []int
		s2 []int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{ "Test 1", args{[]int{0,1}, []int{1,0}}, true },
		{ "Test 2", args{[]int{0,1}, []int{0,1}}, true },
		{ "Test 3", args{[]int{0,1}, []int{1,1}}, false },
		{ "Test 4", args{[]int{0,1}, []int{0,0}}, false },
		{ "Test 5", args{[]int{1,1}, []int{0,0}}, false },
		{ "Test 6", args{[]int{0,0}, []int{1,1}}, false },
		{ "Test 7", args{[]int{1,1}, []int{0,1}}, false },
		{ "Test 8", args{[]int{0,0}, []int{1,0}}, false },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SlicesEqualsByContentInt(tt.args.s1, tt.args.s2); got != tt.want {
				t.Errorf("SlicesEqualsByContentInt() = %v, want %v", got, tt.want)
			}
		})
	}
}
