package helpers

func SlicesEqualsByContentInt(s1, s2 []int) bool {
	m1 := make(map[int]bool)
	for _, v1 := range s1 {
		m1[v1] = false
	}

	m2 := make(map[int]bool)
	for _, v2 := range s2 {
		m2[v2] = false
	}

	for _, v2 := range s2 {
		if _, ok := m1[v2]; !ok {
			return false
		} else {
			m1[v2] = true
		}
	}

	for _, v1 := range s1 {
		if _, ok := m2[v1]; !ok {
			return false
		} else {
			m2[v1] = true
		}
	}

	for _, v1 := range m1 {
		if !v1 {
			return false
		}
	}

	for _, v2 := range m2 {
		if !v2 {
			return false
		}
	}

	return true
}
