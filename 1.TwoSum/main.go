package main

func twoSum(nums []int, target int) []int {
	result := make(map[int]int)
	for i1, n1 := range nums {
		if i2, ok := result[target-n1]; ok {
			return []int{i1, i2}
		}
		result[n1] = i1
	}

	return []int{}
}
