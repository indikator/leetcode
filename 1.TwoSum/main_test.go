package main

import (
	"testing"
	helpers "gitlab.com/indikator/leetcode/0.Helpers"
)

func Test_twoSum(t *testing.T) {
	type args struct {
		nums   []int
		target int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{"Test 1", args{[]int{2, 7, 11, 15}, 9}, []int{1, 0}},
		{"Test 2", args{[]int{3, 2, 4}, 6}, []int{2, 1}},
		{"Test 3", args{[]int{3, 3}, 6}, []int{1, 0}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := twoSum(tt.args.nums, tt.args.target); !helpers.SlicesEqualsByContentInt(got, tt.want) {
				t.Errorf("twoSum() = %v, want %v", got, tt.want)
			}
		})
	}
}
